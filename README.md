# RF IOC Deployment Overview - Confluence table updater

This script reads a pre-existing deployment table like [RF IOC deployment data](https://confluence.esss.lu.se/display/IS/RF+IOC+deployment+data) and updates it with entries for newly deployed IOCs.

# Usage

## Manual

### Environment Setup

Instal the required python libraries

```
pip install -r requirements.txt
```

The input for the script is the file `ioc_list.csv`, which is a simple list of URLs to CCCE deployment pages

```
$ cat ioc_list.csv
https://ccce.esss.lu.se/iocs/480
https://ccce.esss.lu.se/iocs/490
```

### Access Token

To be able to update a Confluence page using the REST API, one must have a personal access token.

Create one by following the guide: https://confluence.atlassian.com/enterprise/using-personal-access-tokens-1026032365.html and save it to a file to supply it to the script

### Usage

Run the script by supplying the following arguments:

```
python3 confluence.py <Confluence Personal Access Token> <Confluece page> ioc_list.csv --output_html <output file>
```

For example, to create a updated HTML page based on the current page in the ICS - WP10 area:

```
python3 confluence.py <Confluence Personal Access Token> https://confluence.esss.lu.se/display/IS/RF+IOC+deployment+data ioc_list.csv
```

Feed the generated HTML file to the deploy script:

```
python3 deploy.py <Confluence Personal Access Token> https://confluence.esss.lu.se/display/IS/RF+IOC+deployment+data <input html file>
```


## Gitlab CI

The Gitlab CI is already configured to build and deploy the updated Confluence page upon commits on the master branch

Just write your IOC list in the `ioc_list.csv` file and commit it to master.

Appending to the file is fine, but this will add a litle bit extra runtime since the script will parse all entries and overwrite them in the table. This file is not meant to be a database of any kind, so overwriting it is preferred.
