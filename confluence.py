from atlassian import Confluence
import argparse
import re
import urllib
import pandas as pd
from bs4 import BeautifulSoup
import requests
import pprint

#Supress warning about chained locs on dataframe
pd.options.mode.chained_assignment = None

pp = pprint.PrettyPrinter(indent=4)

# Links
CCCE_API_URL = 'https://ce-deploy.esss.lu.se/api/v1/iocs/'
CONFLUENCE_URL = 'http://confluence.esss.lu.se'

IocIndexMapping = {
    1 : "LLRF",
    2 : "RFLPS-FIM",
    3 : "EVR",
    4 : "Local Oscillator",
    5 : "Modulator",
    6 : "RFLPS-SIM",
    7 : "IPMI",
    8 : "SCB",
    9 : "Arc Detector",
    10 : "Pin Diode",
    11 : "Electron Pickup",
    12 : "NCL/SCL Pre-Amplifier",
    13 : "Buncher Amplifier",
    14 : "Ion-Pump",
    15 : "Spoke Amplifier",
    16 : "Piezo Tuner",
    17 : "Slow Tuner - Stub Temperature",
    18 : "CCU/TCU",
    19 : "Solenoid PS",
    20 : "Filament PS",
    21 : "Local Oscillator Box",
    22 : "Local Oscillator Split Box",
    23 : "Monitoring Oscilloscope",
    #24 : "Placeholder 4",
    #25 : "Placeholder 5",
    #26 : "Placeholder 6",
    #27 : "Placeholder 7",
    #28 : "Placeholder 8",
    #29 : "Placeholder 9"
}

# regex pattern to match pageId if already in url
page_id_in_url_pattern = re.compile(r"\?pageId=(\d+)")

def getPageIdFromUrl(confluence, url):
    page_url = urllib.parse.unquote(url) #unquoting url to deal with special characters like '%'
    space, title = page_url.split("/")[-2:]

    if re.search(page_id_in_url_pattern, title):
        return re.search(page_id_in_url_pattern, title).group(1)

    else:
        title = title.replace("+", " ")
        return confluence.get_page_id(space, title)

def getNameFromCcce(ioc_id):
    resp = requests.get(CCCE_API_URL+str(ioc_id))
    if resp.status_code == 200:
        return resp.json()['namingName']
    else:
        raise Exception('Error getting name from CCCE API', resp.status_code)

def getRFStationName(sys, subsys, rf_station):
    name = ''
    if sys == 'RFQ':
        name = sys
    elif sys == 'MEBT':
        name = sys+subsys+' - '+rf_station
    elif sys == 'DTL':
        name = sys+subsys
    elif sys == 'Spk':
        name = sys+subsys+' - PS '+ rf_station
    elif sys == 'MBL' or sys == 'HBL':
        name = sys+subsys+' - K'+rf_station
    return name

def insertIocInTable(table, iocname, ioclink):
    if args.debug:
        print(f'\nInserting IOC {iocname} with link {ioclink} in table')

    sys = iocname.split('-')[0]
    subsys = iocname.split('-')[1][0:3]
    rf_station = iocname.split('-')[3][0]
    aux_system = iocname.split('-')[3][-2:]

    if int(aux_system) not in IocIndexMapping.keys():
        print(f'System index "{aux_system}" not identified, skipping')
        return

    if args.debug:
        print(f'sys: {sys}')
        print(f'subsys: {subsys}')
        print(f'rf_station: {getRFStationName(sys,subsys,rf_station)}')
        print(f'aux_system: {IocIndexMapping[int(aux_system)]}')

    if int(rf_station) not in range(1,5):
        return

    #Get row of this RF station
    row = table.loc[:, :, getRFStationName(sys,subsys,rf_station)]
    aux_system_name = IocIndexMapping[int(aux_system)]
    #Set the IOC on its respective column
    link_tag = BeautifulSoup("",'html.parser').new_tag('a', href=ioclink)
    link_tag.string = iocname
    row.loc[:, ('Systems',aux_system_name)] = str(link_tag)

    #Re-insert row into table
    table.loc[:, :, getRFStationName(sys,subsys,rf_station)] = row

parser = argparse.ArgumentParser(description='Get confluence page ID from link')
parser.add_argument('token', help='File containing Personal Access Token', type=argparse.FileType('r'))
parser.add_argument('page_url')
parser.add_argument('ioc_file', type=argparse.FileType('r'))
parser.add_argument('--debug', '-d', action='store_true', help='Enable debug')
parser.add_argument('--html_output', type=str, default='deploy.html')
args = parser.parse_args()

ccce_links = args.ioc_file.read().splitlines()
#Remove empty rows
ccce_links = list(filter(None, ccce_links))
#Remove commented out rows
ccce_links = list(filter(lambda x: x.startswith('#') == False, ccce_links))

#Update CCCE URL if old one is still used
ccce_links = [link.replace("https://ccce.esss.lu.se/", "https://ce-deploy.esss.lu.se/") for link in ccce_links ]

ccce_naming = []
for link in ccce_links:
    ioc_id = link.split('/')[-1]
    if args.debug:
        print(f'IOC ID: {ioc_id}')
    try:
        name = getNameFromCcce(ioc_id)
        if args.debug:
            print(f'IOC Name: {name}')
        ccce_naming.append(name)
    except:
        #IOC not found - Put empty link
        ccce_naming.append('')
        if args.debug:
            print(f'IOC Name not found!')

ccce_info = dict(zip(ccce_naming,ccce_links))
if args.debug:
    print('\nList of IOCs and URL to be inserted:')
    pp.pprint(ccce_info)

#Confluence page loading
token = args.token.read().splitlines()[0]

confluence = Confluence(
    url=CONFLUENCE_URL,
    token=token)

id = getPageIdFromUrl(confluence, args.page_url)
if args.debug:
    print(f'\nConfluence page ID: {id}\n')

page_content = confluence.get_page_by_id(id, expand="body.storage")
page_body = page_content['body']['storage']['value']

html_page = BeautifulSoup(page_body, 'html.parser')
html_tbl = html_page.find('table')
page_tbl = pd.read_html(str(html_tbl),header=[0],index_col=[0,1,2])[0]

#Read all HTML links from table
links = html_tbl.find_all('a')

#Fill back the HTML links into the pandas dataframe
for idx,row in page_tbl.iterrows():
    for col in page_tbl.columns:
        for link in links:
            cell_content = str(page_tbl.at[idx,col])
            if cell_content in link.string:
                if args.debug:
                    print(f'Found string {cell_content} in link table: {link.string}')
                page_tbl.at[idx,col] = str(link).replace("https://ccce.esss.lu.se/", "https://ce-deploy.esss.lu.se/")
                if args.debug:
                    print(f'Replace cell content with {str(page_tbl.at[idx,col])}')
            #Special case for more than one link in a cell (legacy)
            #Expect the two links to be separated by a space (\xa0)
            if '\xa0' in cell_content:
                for subitem in cell_content.split('\xa0'):
                    if subitem in link.string:
                        subitem_tag = html_tbl.find_all("a",string=subitem)
                        print(f'Found subitem {subitem} in cell: {cell_content}')
                        page_tbl.at[idx,col] = cell_content.replace(subitem,str(link))
                        print(f'Cell replaced with: {str(page_tbl.at[idx,col])}')

#Add new IOCs to table
for iocname, ioclink in ccce_info.items():
    insertIocInTable(page_tbl, iocname, ioclink)

#Rebuild HTML table
updated_html = page_tbl.to_html(na_rep='', escape=False)
dump_soup = BeautifulSoup(updated_html, 'html.parser')

#Edit table header
dump_soup.table['class'] = "relative-table wrapped"
del dump_soup.table['border']
dump_soup.table['style'] = "width: 143.925%;"

#Create hollow HTML soup with the parameters from the existing one
hollow = BeautifulSoup(page_body, 'html.parser')
#Remove existing table
hollow.contents[1].table.decompose()

#Append updated table
table_slot = hollow.find("ac:rich-text-body")
table_slot.append(dump_soup)

with open(args.html_output, 'w') as f:
    f.write(str(hollow))
