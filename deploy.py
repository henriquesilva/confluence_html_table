import requests
import argparse
import urllib
import re
from atlassian import Confluence

parser = argparse.ArgumentParser(description='Update Confluence page body')
parser.add_argument('token', help='File containing Personal Access Token', type=argparse.FileType('r'))
parser.add_argument('page_url', help='URL of Confluence page to update')
parser.add_argument('html_data', help='HTML code of the page', type=argparse.FileType('r'))
args = parser.parse_args()

token = args.token.read().splitlines()[0]

# Links
CONFLUENCE_URL = 'http://confluence.esss.lu.se'

# regex pattern to match pageId if already in url
page_id_in_url_pattern = re.compile(r"\?pageId=(\d+)")

def getPageIdFromUrl(confluence, url):
    page_url = urllib.parse.unquote(url) #unquoting url to deal with special characters like '%'
    space, title = page_url.split("/")[-2:]

    if re.search(page_id_in_url_pattern, title):
        return re.search(page_id_in_url_pattern, title).group(1)

    else:
        title = title.replace("+", " ")
        return confluence.get_page_id(space, title)

confluence = Confluence(
    url=CONFLUENCE_URL,
    token=token)

id = getPageIdFromUrl(confluence, args.page_url)

page_content = confluence.get_page_by_id(id, expand="body.storage,version")

#Keep title
title = page_content['title']
api_url = page_content['_links']['self']

#Clear any newlines used to make file pretty
table = args.html_data.read().replace('\n','')

#Get page version (need to send an incremented version)
page_version = page_content['version']['number'] + 1

updated_data = {
    "id": id,
    "version": {
        "number": page_version
    },
    "title": title,
    "type": "page",
    "body": {
        "storage": {
            "value": table,
            "representation": "storage"
        }
    }
}

try:
    response=requests.put(
        api_url,
        headers={"Content-Type":"application/json", "Authorization":str("Bearer "+token)},
        json = updated_data
    )
except:
    raise
